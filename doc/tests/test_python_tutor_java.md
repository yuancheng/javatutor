# This doc aims at testing PythonTutor/Java

There are two sites using it:

* http://www.pythontutor.com/java.html#mode=edit
* http://cscircles.cemc.uwaterloo.ca/java_visualize/#mode=edit

Personally I find that the second site is better made than the first site.
By the time when I tested the program, the first site was out of service,
therefore the following tests are based on the second site.


1. Multiple classes

    We can have only one file! For learning the inheritance in OOP, we can only write all classes in the file.
    This can be done by using inner class technic. For example:

        public class Main {
            public static void main(String[] args) {
                abstract class Animal {
                    public abstract void say();
                }
                class Dog extends Animal {
                    @Override
                    public void say() {
                        System.out.println("Wow!");
                    }
                }
                class Cat extends Animal {
                    @Override
                    public void say() {
                        System.out.println("Miao!");
                    }
                }
            Animal c = new Cat();
            Animal d = new Dog();
            c.say();
            d.say();
            }
        }

    Note that, in this case, the inner class cannot be public!

    If we wish the inner class to be public, it can only be initiated after initiating the outer class.

        public class Main {
            private abstract class Animal {
                public abstract void say();
            }
            private class Dog extends Animal {
                @Override
                public void say() {
                    System.out.println("Wow!");
                }
            }
            class Cat extends Animal {
                @Override
                public void say() {
                    System.out.println("Miao!");
                }
            }
            public static void main(String[] args) {
                Animal c = new Main().new Cat();
                Animal d = new Main().new Dog();
                c.say();
                d.say();
            }
        }


    Otherwise, we must make inner class static, so that we don't need to initiate the outer class.
    However, we still need to prefix the inner class with the outer class's name.

        public class Main {
            public static abstract class Animal {
                public abstract void say();
            }
            public static class Dog extends Animal {
                @Override
                public void say() {
                    System.out.println("Wow!");
                }
            }
            public static class Cat extends Animal {
                @Override
                public void say() {
                    System.out.println("Miao!");
                }
            }
            public static void main(String[] args) {
                Animal c = new Main.Cat();
                Animal d = new Main.Dog();
                c.say();
                d.say();
            }
        }

    Time need before visualization: none of previous snippets takes significantly long for generating the result.

    To conclude, we can still learn the inheritance of OOP, but this static inner class mechanism
    may not be obvious for beginners.


2. MultiThreading

    Test code:

        public class MultiThread {
            public static class MyThread extends Thread {
                @Override
                public void run() {
                    System.out.println("runnable!!!!");
                }
            }
            public static void main(String[] args) {
                Thread t = new MyThread();
                t.start();
                System.out.println("hello");
            }
        }

    Multi-threading is not supported in `PythonTutor/Java` because it assumes one thread!
    After 15 seconds of execution(timeout), these information appears in a popup windows.


3. Maximum steps

    Let's hit the limit of `PythonTutor/Java`!

        public class Test {
            public static void main(String[] args) {
                for (int i = 0; i < 1000; i++) {
                    int k = i * i;
                }
            }
        }

    This code does nothing in output but takes more than 3000 steps to execute.
    `PythonTutor/Java` can generate the traces but under limited steps.
    (510 steps on http://cscircles.cemc.uwaterloo.ca/java_visualize/#mode=display)

    Time: we hit the steps limit, but it's still finished in an acceptable time(less than 10 seconds).

    We also test this code directly on its backend `java_jail` and we notice that the compilation time
    is stable no matter how big the loop is. However, a larger loop results in a longer execution time
    and a longer JSON string.


4. Stack overflow

        public class StackOverflow {
            private static void burn(int i) {
                i = i + 1;
                burn(i);
            }
            public static void main(String[] args) {
                burn(0);
            }
        }

    This code calls the `burn` method recursively.
    `PythonTutor/Java` can trace until `i = 14`, then we are told that the code exceeds the visualizer's stack size.
