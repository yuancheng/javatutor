# IDE

Download the STS (Spring Tool Suite), then extract it.
Go into the `sts-bundle/sts-[version].RELEASE` directory, and run `./STS`

**install gradle**

Go to `Dashboard page`, which is prompt everytime when STS is started.
Select `Extensions` tab on the buttom.
Search `gradle` in the search field.
Install `Gradle Support`.


**create spring starter project**

File -> New -> Sprint Starter Project.
Type -> "Gradle project" -> Next.
Web -> Web -> Finish.

Now you can start the demo project:
Find the main class in the `Package Explorer`.
Highlight the class name and click the run button, run as `Sprint Boot App`.

On the browser, goto `http://localhost:8080`.
You can see an error message of the app.
Good, the app is running, but nothing is developped.
You are ready to go!


**libraries**

Include `jdi` and `javax.json-1.0.jar`:

Properties -> Java Build Path -> Libraries -> Add External JARs.

select `/usr/lib/jvm/java-8-openjdk-amd64/lib/tools.jar`,
then `javax.json-1.0.jar` under the `libs/` directory.

# CLI

**Build and run**

Build the project with `gradle`:

    $> gradle build

Run the project

    $> java -jar build/libs/<app_name>.jar

The jar file name is configured in `build.gradle`, in the section `jar`.
