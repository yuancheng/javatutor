# Build the project starting from zero

This section shows you how to build the project with the source code,
and deploy it on a Linux server.

Let's start this with a brand new `Ubuntu 14.04 TLS` installed on virtualbox.

Note that you must allocate at least 2 processors to your virtual machine,
otherwise when the backend tracer will fail to launch JVM.

Some basic softwares that you need on your server:

    $> sudo apt-get install git

**Java 8**

    $> sudo add-apt-repository ppa:openjdk-r/ppa
    $> sudo apt-get update
    $> sudo apt-get install openjdk-8-jdk
    $> sudo update-alternatives --config java
    $> sudo update-alternatives --config javac

Check your java version: `java -version`.


**Gradle**

Install gradle by following [this link](https://docs.gradle.org/current/userguide/installation.html).

Do not use `apt-get` to install gradle because we need a more recent version.


**Get the project and build the backend**

    $> git clone https://yuancheng@bitbucket.org/fdagnat/javatutor.git
    $> cd javatutor/backend/
    $> gradle build

The built jar file will be placed in `build/libs/<app_name.jar>`.


**Build the frontend**

To build the frontend, we need `npm` and some dependencies:

    $> sudo apt-get install npm
    $> sudo apt-get install nodejs-legacy

    $> sudo npm install -g bower
    $> sudo npm install -g grunt-cli


That's it. Let's build it.

    $> cd javatutor/frontend/
    $> sudo npm install
    $> bower install
    $> grunt build --force

The built frontend app can be found in `javatutor/frontend/dist/`.

Open the `index.html` page to access to app.

You might notice that the code on the ace editor is not highlighted.
That's because the build script didn't include it, so you need to found out that
js file and copy to the `dist/`:

    $> cp bower_components/ace-builds/src-min-noconflict/mode-java.js dist/

Reopen the `index.html` to ensure that it works well.


**Put everthing together**

Now that we have a `jar` file for the backend and a `dist` directory that
contains a frontend app, we can deploy it on a web server like nginx.

Install nginx

      $> sudo apt-get install nginx


Configure the nginx, edit `/etc/nginx/sites-enabled/default`,
change the `root` of the default server to your `javatutor/frontend/dist/`
(you need to replace it by absolute path).

Restart nginx:

    $> sudo service nginx restart

Make sure that your backend is running:

    $> java -jar build/libs/<app_name.jar>


Test it: open your browser, go to `http://localhost`.


# Production

(we have not yet distributed the app on a public server)
TODO


# Q&A


## Screen Resolution Problem on Virtual box

Your screen resolution may stuck to 640x480 after installing Ubuntu 14.04 TLS
using virtual box.

To solve this problem, on your virtual machine, run the following commands:

    $> sudo apt-get remove libcheese-gtk23
    $> sudo apt-get install xserver-xorg-core
    $> sudo apt-get install -f virtualbox-guest-x11

ref: [see here](http://askubuntu.com/questions/451805/screen-resolution-problem-with-ubuntu-14-04-and-virtualbox)
