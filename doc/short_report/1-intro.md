# What is JavaTutor?

JavaTutor is a tool for visualizing the execution of Java programs on the Web.

It aims at helping people to learn object-oriented programming (OOP) by showing what computer does
when executing each line of a Java program's source code.

# Context & Background

**Motivation**

The Institut Mines-Telecom is now developing online courses for learning the object-oriented programming.
In this context, we need a tool to visualize the execution of a Java program step by step, which helps better
understanding the mechanisms of objects of an object-oriented program.


On the other hand, this is the project of my 5th semester at Telecom Bretagne.


**Existed tools**

As a matter of fact, the famous [Python Tutor](http://www.pythontutor.com) which helps to learn Python
also offers the visualization of Java programs.

This tool is available in the following two sites:

* http://www.pythontutor.com/java.html
* http://cscircles.cemc.uwaterloo.ca/java_visualize/

For the sake of distinguishing this tool from ours, let us call that `PythonTutor/Java`, and ours `JavaTutor`.

**Why reinventing the wheel?**

The `PythonTutor/Java`, however, does not satisfy all the functionalities
that we initially imagined.

In the [next chapter](./2-analysis-of-pythontutor-java.md), we will analyze this tool and clarify our anticipated features.

