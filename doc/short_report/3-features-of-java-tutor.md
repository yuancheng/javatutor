# Our ideas / features

**Features**

Here are the features we need:

* Visualize the execution step by step of a Java program.

* The visualization provides the following information:

    - A code area indicating the next line to execute
    - Call stacks of different threads
    - Variables in frames
    - Different detail levels of the objects
    - A console for output

* Control during the visualization

    - step into a function call
    - step over a function call
    - step back to previsou call

* Can be easily installed locally. (Linux/Windows)

* Can be easily embedded into other Websites

**Scenario**

Here is basic idea of a visualization scenario:

1. We write some Java classes (including the main class) with on a web-based text editor.
2. We click `start visualization`, so the code is sent to the server.
2. We start executing the program step by step.
3. We can step into/over a function call.
4. During the execution, we can see the objects (and also objects inside it if we want more details).
5. A console to show the executions results if they are printed out.


**Compare with `PythonTutor/Java`**

To summarize the previous analysis,
compared with `PythonTutor/Java`, `JavaTutor` has the following enhancement/features:

*1. Enhancement*

* Much more steps of execution
* Less waiting time before the visualization

*2. New Features*

* Multiple files for multiple classes
* Step-in / step-over control
* Configuration of objects' detail level during the visualization
* Multi-threading support

The stdin rests the same as `PythonTutor/Java`

## Limits

Of cause, even though we support more steps of execution, we will still consider some limits to:

* prevent the infinite loop.
* prevent stack overflow. (e.g. recursive call)


[next chapter](./4-design.md)

