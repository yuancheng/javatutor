package com.webserve;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import traceprinter.InMemory;

@RestController
public class VMRequestController {

	@CrossOrigin
	@RequestMapping(value = "/code", method = RequestMethod.POST)
	public String readCode (@RequestBody MultiClassVisualizeRequest vr, HttpServletResponse response) {

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			String json = ow.writeValueAsString(vr);
			String jsonOutput = InMemory.runMultifilesInstance(json);
			if (jsonOutput != null)
				return jsonOutput;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return "{'error': 'something wrong with the server!'}";
	}

}