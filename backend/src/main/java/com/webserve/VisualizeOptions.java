package com.webserve;

public class VisualizeOptions {

	private boolean istest;

	public boolean isIstest() {
		return istest;
	}

	public void setIstest(boolean istest) {
		this.istest = istest;
	}

	public VisualizeOptions() {
	}

	public VisualizeOptions(boolean istest) {
		super();
		this.istest = istest;
	}

}
