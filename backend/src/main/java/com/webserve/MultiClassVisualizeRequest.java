package com.webserve;

import java.util.List;

public class MultiClassVisualizeRequest {

	// each array item represents a Java file
	private String [] usercode;

	private String stdin;

	private VisualizeOptions options;

	private List<String> args;

	public MultiClassVisualizeRequest() {

	}

	public MultiClassVisualizeRequest(String [] usercode, String stdin, VisualizeOptions options, List<String> args) {
		this.usercode = usercode;
		this.stdin = stdin;
		this.options = options;
		this.args = args;
	}

	public String getStdin() {
		return stdin;
	}

	public void setOptions(VisualizeOptions options) {
		this.options = options;
	}

	public String [] getUsercode() {
		return usercode;
	}

	public void setUsercode(String [] usercode) {
		this.usercode = usercode;
	}

	public void setStdin(String stdin) {
		this.stdin = stdin;
	}

	public VisualizeOptions getOptions() {
		return options;
	}

	public List<String> getArgs() {
		return args;
	}

	public void setArgs(List<String> args) {
		this.args = args;
	}

}
