/**
 * Created by Yuancheng Peng, March 2016
 */


'use strict';

angular.module('frontendApp')
  .factory('jtPlumb', ['$window', function(win) {

    /* colors */
    var brightRed = '#e93f34';

    var connectorBaseColor = '#005583';
    var connectorHighlightColor = brightRed;

    var activeColor = connectorBaseColor;
    var inactiveColor = 'grey';

    return {
      createConn: function(source, target, options) {
        // nasty jquery
        jsPlumb.setContainer($("#plumb-container")); // id is hard coded in stack_heap.html

        options = options || {};
        var _options = {
          source: source,
          target: target,
          connector: ["StateMachine"],
          overlays: [[ "Arrow", { length: 10, width:7, foldback:0.55, location:1 }]],
          anchors:["Center", ["Left", "Right", "Top"]],
          endpoint: [ "Dot", { radius:3 } ],
          endpointHoverStyles: [{fillStyle: connectorHighlightColor}, {fillStyle: ''} /* make right endpoint invisible */],

          paintStyle: { lineWidth:1, strokeStyle: connectorBaseColor},
          endpointStyles: [{fillStyle: connectorBaseColor}, {fillStyle: ''} /* make right endpoint invisible */],
          hoverPaintStyle: {lineWidth: 1, strokeStyle: connectorHighlightColor}
        };
        var attr;
        for (attr in options) {
          _options[attr] = options[attr];
        }

        jsPlumb.connect(_options);
      },

      createActiveConn: function (source, target) {
        this.createConn(source, target, {
          paintStyle:{
            strokeStyle:activeColor, lineWidth:1.5, // a thicker connector
            // set an invisible border for better mouse hovering
            outlineColor:"rgba(255,255,255,0)", outlineWidth:10
          },
          hoverPaintStyle: {lineWidth: 1.5, strokeStyle: connectorHighlightColor},
          endpointStyles: [{fillStyle: activeColor}, {fillStyle: ''}]
        });
      },

      createInactiveConn: function (source, target) {
        this.createConn(source, target, {
          paintStyle:{
            strokeStyle:inactiveColor, lineWidth:1,
            // set an invisible border for better mouse hovering
            outlineColor:"rgba(255,255,255,0)", outlineWidth:10
          },
          endpointStyles: [{fillStyle: inactiveColor}, {fillStyle: ''}]
        });
      },

      reset: function () {
        jsPlumb.reset(); // reset to initial state, which avoids a lot of issues
      }
    };
  }])

  .factory('jtHelper', function () {
    return {
      /*
      Make a copy of the data, show the strings as primitive values
      instead of objects.
       */
      inlineString: function (data) {
        // clone object
        var newData = JSON.parse(JSON.stringify(data));
        var i, k, t, s, h;
        var stack, heap, staticFields;
        var strings, key, item;
        var encoded_locals;

        for (i = 0; i < newData.trace.length; i++) {
          t = newData.trace[i];
          stack = t.stack_to_render;
          heap = t.heap;
          staticFields = t.globals;
          strings = {};
          for (key in heap) {
            if (heap[key][0] === "HEAP_PRIMITIVE" && heap[key][1] === "String") {
              strings[key] = heap[key][2];
            }
          }

          for (key in strings) {
            delete heap[key];
          }

          // remove stack string reference by value
          for (s in stack) { // stack is an array
            encoded_locals = stack[s]["encoded_locals"];
            for (key in encoded_locals) {
              if (encoded_locals[key]
                && encoded_locals[key][0] === "REF"
                && encoded_locals[key][1] in strings) {
                // replace the reference by the string value.
                encoded_locals[key] = strings[encoded_locals[key][1]];
              }
            }
          }

          // Do the same thing with static fields
          for (key in staticFields) {
            item = staticFields[key];
            if (item && item[0] === "REF" && item[1] in strings) {
              staticFields[key] = strings[item[1]];
            }
          }

          // Do the same thing with heap objects
          for (key in heap) {
            item = heap[key];
            if (item && item[0] === "INSTANCE") {
              for (k = 2; k < item.length; k++) {
                // item[k] is a field of an instance, item[k][1] is the reference array
                if (item[k][1] && item[k][1][0] === "REF" && item[k][1][1] in strings) {
                  // replace the ref by string value
                  item[k][1] = strings[item[k][1][1]];
                }
              }
            }

            else if (item && item[0] == "LIST") {
              for (k = 1; k < item.length; k++) {
                if (item[k] && item[k][0] === "REF" && item[k][1] in strings) {
                  item[k] = strings[item[k][1]];  // replace
                }
              }
            }
          }
        }
        return newData;
      } // end of function

    }; // end of return object
  })
;
