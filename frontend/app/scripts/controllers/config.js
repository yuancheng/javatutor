/**
 * Created by Yuancheng Peng, March 2016
 */


'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ConfigCtrl
 * @description
 * # ConfigCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('ConfigCtrl',
    ['$scope', 'localStorageService',
    function ($scope, localStorageService) {
      var STORAGE_URL_KEY = 'server_url';

      $scope.server_url = localStorageService.get(STORAGE_URL_KEY)
        || "http://localhost:8080";

      $scope.save_url = function (url) {
        localStorageService.set(STORAGE_URL_KEY, url);
        $scope.info = "Server URL updated: " + url;
      }
    }]
  )
;
